<?php
    class Usuario{
        public $nome;
        private $cpf;
        private $endereco;
    

        function __construct($nome_externo, $cpf, $endereco){
            $this->nome = $nome_externo;
            $this->cpf = $cpf;
            $this->endereco = $endereco;
        }


        function getCpf(){
            return $this->cpf;
        }

        function getEndereco(){
            return $this->endereco;
        }
    }

?>
